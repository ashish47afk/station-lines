import FusionCharts from "fusioncharts";
import charts from "fusioncharts/fusioncharts.charts";
import ReactFusioncharts from "react-fusioncharts";
import React from 'react';
import PowerCharts from 'fusioncharts/fusioncharts.powercharts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import ReactFC from 'react-fusioncharts';
import "./style.css";

ReactFC.fcRoot(FusionCharts, PowerCharts, FusionTheme);

// Resolves charts dependancy
charts(FusionCharts);

const dataSource = {
  chart: {
    xaxisminvalue: "0",
    xaxismaxvalue: "10",
    yaxisminvalue: "110",
    yaxismaxvalue: "-90",
    viewmode: "0",
    showplotborder: "2",
    plotbordercolor: "#0000",
    plotborderthickness: "4",
    showtooltip: "0",
    theme: "fusion",
    plotborderalpha: "100",
    divlinealpha: "0",
    allowDrag: "1"
  },
  dataset: [
    {
      data: [
        {
          id: "you",
          color: "#ffffa1",
          x: "1",
          y: "-10",
          radius: "15",
          shape: "circle"
        },
        {
          id: "corejava",
          color: "#ffffa1",
          x: "2.5",
          y: "-10",
          radius: "8",
          shape: "circle"
        },
        {
          id: "javajee",
          color: "#ffffa1",
          x: "3.5",
          y: "-10",
          radius: "8",
          shape: "circle"
        },
        {
          id: "level1training",
          color: "#ffffa1",
          x: "4.5",
          y: "-10",
          radius: "12",
          shape: "circle"
        },
        {
          id: "nodejs",
          color: "#ffffa1",
          x: "5.5",
          y: "-10",
          radius: "8",
          shape: "circle"
        },
        {
          id: "html5",
          color: "#ffffa1",
          x: "6.5",
          y: "-10",
          radius: "8",
          shape: "circle"
        },
        {
          id: "level2training",
          color: "#ffffa1",
          x: "6.5",
          y: "-7",
          radius: "12",
          shape: "circle"
        },
        {
          id: "frontenddeveloper",
          color: "#ffffa1",
          x: "5.5",
          y: "-7",
          radius: "8",
          shape: "circle"
        },
        {
          id: "level3training",
          color: "#ffffa1",
          x: "4.5",
          y: "-7",
          radius: "12",
          shape: "circle"
        },
        {
          id: "tester1",
          color: "#ffcba4",
          x: "3.5",
          y: "-7",
          radius: "8",
          shape: "circle"
        },
        {
          id: "tester2",
          color: "#ffcba4",
          x: "2.5",
          y: "-7",
          radius: "8",
          shape: "circle"
        },
        {
          id: "level4training",
          color: "#ffcba4",
          x: "2.5",
          y: "-4",
          radius: "12",
          shape: "circle"
        },
        {
          id: "qa",
          color: "#ffcba4",
          x: "1.8",
          y: "-4",
          radius: "8",
          shape: "circle"
        },
        {
          id: "programlead",
          color: "#ffcba4",
          x: "1",
          y: "-4",
          radius: "8",
          shape: "circle"
        },
        {
          id: "mvpprogram",
          color: "#ffcba4",
          x: "1",
          y: "-7",
          radius: "8",
          shape: "circle"
        },
        {
          id: "sql",
          color: "#ffcba4",
          x: "7.5",
          y: "-7",
          radius: "8",
          shape: "circle"
        },
        {
          id: "mysql",
          color: "#ffcba4",
          x: "8.5",
          y: "-7",
          radius: "8",
          shape: "circle"
        },
        {
          id: "leaddeveloper",
          color: "#ffcba4",
          x: "9.5",
          y: "-7",
          radius: "8",
          shape: "circle"
        },
        {
          id: "b1",
          color: "#ffcba4",
          x: "9.5",
          y: "-4",
          radius: "8",
          shape: "circle"
        },
        {
          id: "sme",
          color: "#ffcba4",
          x: "9.5",
          y: "-2",
          radius: "8",
          shape: "circle"
        },
        {
          id: "majentolead",
          color: "#ffcba4",
          x: "8.5",
          y: "-2",
          radius: "8",
          shape: "circle"
        },
        {
          id: "hadooplead",
          color: "#ffcba4",
          x: "7.5",
          y: "-2",
          radius: "8",
          shape: "circle"
        },
        {
          id: "b2",
          color: "#ffcba4",
          x: "8.5",
          y: "-4",
          radius: "8",
          shape: "circle"
        },
        {
          id: "b3",
          color: "#ffcba4",
          x: "7.5",
          y: "-4",
          radius: "8",
          shape: "circle"
        },
        {
          id: "b4",
          color: "#ffcba4",
          x: "6.5",
          y: "-4",
          radius: "8",
          shape: "circle"
        },
        {
          id: "b5",
          color: "#ffcba4",
          x: "4.5",
          y: "-4",
          radius: "8",
          shape: "circle"
        },
        {
          id: "fullstackdeveloper",
          color: "#ffcba4",
          x: "4.5",
          y: "-2",
          radius: "8",
          shape: "circle"
        },
        {
          id: "b6",
          color: "#ffcba4",
          x: "2.5",
          y: "-2.9",
          radius: "8",
          shape: "circle"
        },
        {
          id: "projectmanager",
          color: "#ffcba4",
          x: "2.5",
          y: "-2",
          radius: "8",
          shape: "circle"
        },
      ]
    }
  ],
  connectors: [
    {
      stdthickness: "6",
      connector: [
        {
          from: "you",
          to: "corejava",
          color: "#FFFF00",
          arrowatstart: "0",
          arrowatend: "corejava"
        },
        {
          from: "corejava",
          to: "javajee",
          color: "#FFFF00",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "javajee",
          to: "level1training",
          color: "#FFFF00",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "level1training",
          to: "nodejs",
          color: "#FFFF00",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "nodejs",
          to: "html5",
          color: "#FFFF00",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "html5",
          to: "level2training",
          color: "#FFFF00",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "level2training",
          to: "frontenddeveloper",
          color: "#FFFF00",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "level2training",
          to: "sql",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "sql",
          to: "mysql",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "mysql",
          to: "leaddeveloper",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "leaddeveloper",
          to: "b1",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "b1",
          to: "sme",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "b2",
          to: "majentolead",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "b3",
          to: "hadooplead",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "b5",
          to: "fullstackdeveloper",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "b6",
          to: "projectmanager",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "level4training",
          to: "b6",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "level4training",
          to: "qa",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "qa",
          to: "programlead",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "programlead",
          to: "mvpprogram",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "you",
          to: "mvpprogram",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "mvpprogram"
        },
        {
          from: "level4training",
          to: "tester2",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "tester2",
          to: "tester1",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "tester1",
          to: "level3training",
          color: "#FFFF00",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "level3training",
          to: "frontenddeveloper",
          color: "#FFFF00",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "level3training",
          to: "level1training",
          color: "#FFFF00",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "level3training",
          to: "b5",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "level2training",
          to: "b4",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "b4",
          to: "b3",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        },
        {
          from: "b3",
          to: "b2",
          color: "#FECC17",
          arrowatstart: "0",
          arrowatend: "0"
        }
      ]
    }
  ],
  labels: {
    label: [
     
      {
        text: "You",
        x: "1",
        y: "-10.7",
        fontsize: "14"
      },
      {
        text: "Core <br /> Java",
        x: "2.5",
        y: "-10.7",
        fontsize: "10"
      },
      {
        text: "Java JEE",
        x: "3.5",
        y: "-10.6",
        fontsize: "10"
      },
      {
        text: "Level 1 <br /> Training",
        x: "4.5",
        y: "-10.68",
        fontsize: "10"
      },
      {
        text: "Node JS",
        x: "5.5",
        y: "-10.5",
        fontsize: "10"
      },
      {
        text: "HTML5",
        x: "6.5",
        y: "-10.5",
        fontsize: "10"
      },
      {
        text: "Level 2 <br /> Training",
        x: "6.3",
        y: "-7.8",
        fontsize: "10"
      },
      {
        text: "SQL",
        x: "7.5",
        y: "-6.5",
        fontsize: "10"
      },
      {
        text: "MySQL",
        x: "8.5",
        y: "-6.5",
        fontsize: "10"
      },
      {
        text: "Lead <br /> Developer",
        x: "9.7 ",
        y: "-6.5",
        fontsize: "10"
      },
      {
        text: "SME",
        x: "9.5",
        y: "-1.5",
        fontsize: "10"
      },
      {
        text: "Majento <br /> Lead",
        x: "8.5",
        y: "-1.4",
        fontsize: "10"
      },
      {
        text: "Hadoop <br /> Lead",
        x: "7.5",
        y: "-1.4",
        fontsize: "10"
      },
      {
        text: "Full Stack <br /> Developer",
        x: "4.5",
        y: "-1.1",
        fontsize: "10",
        borderColor: "#0000"
      },
      {
        text: "Project Manager",
        x: "2.5",
        y: "-1.3",
        fontsize: "10",
        borderColor: "#0000"
      },
      {
        text: "Program Lead",
        x: "1",
        y: "-3.3",
        fontsize: "10",
        borderColor: "#0000"
      },
      {
        text: "QA",
        x: "1.8",
        y: "-3.5",
        fontsize: "10"
      },
      {
        text: "MVP <br /> Program",
        x: "0.78",
        y: "-7",
        fontsize: "10"
      },
      {
        text: "Level 4 <br /> Training",
        x: "2.3",
        y: "-4.5",
        fontsize: "10"
      },
      {
        text: "Tester 2",
        x: "2.28",
        y: "-7",
        fontsize: "10"
      },
      {
        text: "Tester 1",
        x: "3.5",
        y: "-6.5",
        fontsize: "10"
      },
      {
        text: "Level 3 <br /> Training",
        x: "4.3",
        y: "-7.5",
        fontsize: "10"
      },
      {
        text: "Front End <br /> Developer",
        x: "5.5",
        y: "-6.3",
        fontsize: "10"
      },
    ]
  }
};

class MyComponent extends React.Component {
  render() {
    return (
      <div className="back-chart">
        <ReactFusioncharts
        type="dragnode"
        width="100%"
        height="2000%"
        dataFormat="JSON"
        dataSource={dataSource}
      />
      </div>
    );
  }
}

export default MyComponent;